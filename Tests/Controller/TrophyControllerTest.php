<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 26/02/16
 * Time: 18:17
 */

namespace eezeecommerce\RestBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class TrophyControllerTest
 * @package eezeecommerce\RestBundle\Tests\Controller
 * @inheritdoc
 */
class TrophyControllerTest extends WebTestCase
{

    private static $product = array();

    public function testAllWithNoProductsAction()
    {
        $client = static::createClient();

        $crawler = $client->request("GET", "/json/trophy");
        $response = $client->getResponse();
        $this->assertEquals($response->getStatusCode(), 204);
    }

    public function testAddingProductWithPostMethod()
    {
        $client = static::createClient();

        $array = '{"trophy":{"code": "ep001", "description": "ep001 desc", "price": 1.99, "supplier": "epc"}}';

        $crawler = $client->request(
            "POST",
            "/json/trophy",
            array(),
            array(),
            array("CONTENT_TYPE" => "application/json"),
            $array
        );
        $response = $client->getResponse();
        $this->assertJsonResponse($response, 201);

        $this->assertEquals($response->headers->get("Location"), "http://localhost/json/trophy/ep001");
    }

    public function testAddingDuplicateProductShowsError()
    {
        $client = static::createClient();

        $array = '{"trophy":{"code": "ep001", "description": "ep001 desc", "price": 1.99, "supplier": "epc"}}';

        $crawler = $client->request(
            "POST",
            "/json/trophy",
            array(),
            array(),
            array("CONTENT_TYPE" => "application/json"),
            $array
        );
        $response = $client->getResponse();
        $this->assertJsonResponse($response, 400);

    }

    public function testShowAllWithProduct()
    {
        $client = static::createClient();

        $crawler = $client->request("GET", "/json/trophy");
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testAddingProductWithNoData()
    {
        $client = static::createClient();

        $crawler = $client->request(
            "POST",
            "/json/trophy"
        );
        $response = $client->getResponse();
        $this->assertJsonResponse($response, 400);
    }

    public function testAddProductWithInvalidJson()
    {
        $client = static::createClient();

        $array = '{"trophy":{"code": "ep001", "description": "ep001 desc", "price": 1.99, supplier": "epc"}}';

        $crawler = $client->request(
            "POST",
            "/json/trophy",
            array(),
            array(),
            array("CONTENT_TYPE" => "application/json"),
            $array
        );
        $response = $client->getResponse();
        $this->assertJsonResponse($response, 400);

    }

    public function testAddingProductWithInvalidData()
    {
        $client = static::createClient();

        $array = '{"trophy":{"code": "ep001"}}';

        $crawler = $client->request(
            "POST",
            "/json/trophy",
            array(),
            array(),
            array("CONTENT_TYPE" => "application/json"),
            $array
        );
        $response = $client->getResponse();
        $this->assertJsonResponse($response, 400);
    }

    public function testShowingProductAfterPostMethod()
    {
        $client = static::createClient();

        $crawler = $client->request("GET", "/json/trophy/ep001");
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testEditingInvalidProduct()
    {
        $client = static::createClient();

        $array = '{"trophy":{"code": "ep001"}}';

        $crawler = $client->request(
            "PUT",
            "/json/trophy/ep00adf52f1sd65asdf",
            array(),
            array(),
            array("CONTENT_TYPE" => "application/json"),
            $array
        );

        $response = $client->getResponse();
        $this->assertJsonResponse($response, 404);
    }

    public function testEditingProductWithInvalidData()
    {
        $client = static::createClient();

        $array = '{"trophy":{"code": "ep001"}}';

        $crawler = $client->request(
            "PUT",
            "/json/trophy/ep001",
            array(),
            array(),
            array("CONTENT_TYPE" => "application/json"),
            $array
        );

        $response = $client->getResponse();
        $this->assertJsonResponse($response, 400);
    }

    public function testEditingProductWithNoData()
    {
        $client = static::createClient();

        $crawler = $client->request(
            "PUT",
            "/json/trophy/ep001"
        );

        $response = $client->getResponse();
        $this->assertJsonResponse($response, 400);
    }

    public function testEditingProductWithPutMethod()
    {
        $client = static::createClient();

        $array = '{"trophy":{"code": "ep001", "description": "ep001 desc now", "price": 1.99, "supplier": "epc"}}';

        $crawler = $client->request(
            "PUT",
            "/json/trophy/ep001",
            array(),
            array(),
            array("CONTENT_TYPE" => "application/json"),
            $array
        );

        $response = $client->getResponse();
        $this->assertJsonResponse($response, 200);
    }

    public function testDeletingProductWithDeleteMethod()
    {
        $client = static::createClient();

        $crawler = $client->request(
            "DELETE",
            "/json/trophy/ep001"
        );

        $response = $client->getResponse();
        $this->assertEquals($response->getStatusCode(), 204);
    }

    public function testDeletingInvalidProductWithDeleteMethod()
    {
        $client = static::createClient();

        $crawler = $client->request(
            "DELETE",
            "/json/trophy/ep001123123123123"
        );

        $response = $client->getResponse();
        $this->assertEquals($response->getStatusCode(), 404);
    }

    public function testTryAccessingDeletedProduct()
    {
        $client = static::createClient();

        $crawler = $client->request("GET", "/json/trophy/ep001");
        $response = $client->getResponse();
        $this->assertEquals($response->getStatusCode(), 404);
    }

    public function testAddingChildProductTheParent()
    {
        $client = static::createClient();

        $array = '{"trophy":{"code": "ep001", "description": "ep001 desc", "price": 1.99, "supplier": "epc"}}';

        $crawler = $client->request(
            "POST",
            "/json/trophy",
            array(),
            array(),
            array("CONTENT_TYPE" => "application/json"),
            $array
        );

        $response = $client->getResponse();
        $this->assertJsonResponse($response, 201);

        $location = $response->headers->get("Location");

        $crawler = $client->request(
            "GET",
            $location
        );

        $array = json_decode($client->getResponse()->getContent(), true);

        $id = $array['id'];

        $array = '{"trophy":{"code": "ep00asd1", "description": "ep001 desc", "price": 1.99, "supplier": "epc", "parent": {"id": '.$id.'}}}';

        $crawler = $client->request(
            "POST",
            "/json/trophy",
            array(),
            array(),
            array("CONTENT_TYPE" => "application/json"),
            $array
        );

        $response = $client->getResponse();
        $this->assertJsonResponse($response, 201);

    }

    public function testDeleteChildFirst()
    {
        $client = static::createClient();

        $crawler = $client->request(
            "DELETE",
            "/json/trophy/ep001"
        );

        $response = $client->getResponse();
        $this->assertEquals($response->getStatusCode(), 409);
    }

    public function testDeleteChildThenParentProduct()
    {
        $client = static::createClient();

        $crawler = $client->request(
            "DELETE",
            "/json/trophy/ep00asd1"
        );

        $response = $client->getResponse();

        $this->assertEquals(204, $response->getStatusCode());

        $crawler = $client->request(
            "DELETE",
            "/json/trophy/ep001"
        );

        $response = $client->getResponse();
        $this->assertEquals($response->getStatusCode(), 204);

    }

    protected function assertJsonResponse($response, $statusCode = 200)
    {
        $this->assertEquals(
            $statusCode, $response->getStatusCode(),
            $response->getContent()
        );

        $this->assertTrue(
            $response->headers->contains('Content-Type', 'application/json'),
            $response->headers
        );
    }

}