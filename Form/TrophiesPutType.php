<?php

namespace eezeecommerce\RestBundle\Form;

use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Form\Transformer\EntityToIdObjectTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TrophiesPutType extends AbstractType
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $productTransformer = new EntityToIdObjectTransformer($this->em, "eezeecommerceRestBundle:Trophies");

        $builder
            ->setMethod("PUT")
            ->add('code')
            ->add('description')
            ->add('height', null, array(
                "required"=> false
            ))
            ->add('length', null, array(
                "required"=> false
            ))
            ->add('width', null, array(
                "required"=> false
            ))
            ->add('weight', null, array(
                "required"=> false
            ))
            ->add('price', null, array(
                "required"=> false
            ))
            ->add('has_stock')
            ->add('stock')
            ->add('supplier')
            ->add($builder->create("parent", "text")->addModelTransformer($productTransformer))
            ->add("image", null, array(
                "required" => false
            ));
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\RestBundle\Entity\Trophies',
            'csrf_protection' => false
        ));
    }

    public function getName()
    {
        return "trophy";
    }
}
