<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29/02/16
 * Time: 12:41
 */

namespace eezeecommerce\RestBundle\Form\Example;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\RestBundle\Entity\Trophies',
            'csrf_protection' => false
        ));
    }

    public function getName()
    {
        return "parent";
    }
}