<?php

namespace eezeecommerce\RestBundle\Form\Example;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductType
 * @package eezeecommerce\RestBundle\Form
 * {@inheritdoc}
 */
class TrophiesType extends AbstractType
{

    /**
     * {@inheritdoc}
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code')
            ->add('description')
            ->add('height', null, array(
                "required"=> false
            ))
            ->add('length', null, array(
                "required"=> false
            ))
            ->add('width', null, array(
                "required"=> false
            ))
            ->add('weight', null, array(
                "required"=> false
            ))
            ->add('price')
            ->add('has_stock')
            ->add('stock')
            ->add('supplier')
            ->add('parent', new ParentType(), array(
                "required" => false
            ))
            ->add("image", null, array(
                "required" => false
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\RestBundle\Entity\Trophies',
            'csrf_protection' => false
        ));
    }

    public function getName()
    {
        return "trophy";
    }
}
