<?php

namespace eezeecommerce\RestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Form\Transformer\EntityToIdObjectTransformer;

/**
 * Class ProductType
 * @package eezeecommerce\RestBundle\Form
 * {@inheritdoc}
 */
class TrophiesType extends AbstractType
{

    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $productTransformer = new EntityToIdObjectTransformer($this->em, "eezeecommerceRestBundle:Trophies");

        $builder
            ->add('code')
            ->add('description')
            ->add('height', null, array(
                "required"=> false
            ))
            ->add('length', null, array(
                "required"=> false
            ))
            ->add('width', null, array(
                "required"=> false
            ))
            ->add('weight', null, array(
                "required"=> false
            ))
            ->add('price', null, array(
                "required"=> false
            ))
            ->add('supplier')
            ->add('has_stock')
            ->add('stock')
            ->add($builder->create("parent", "text")->addModelTransformer($productTransformer))
            ->add("image", null, array(
                "required" => false
            ));

        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\RestBundle\Entity\Trophies',
            'csrf_protection' => false
        ));
    }

    public function getName()
    {
        return "trophy";
    }
}
