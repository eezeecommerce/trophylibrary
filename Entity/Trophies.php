<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 26/02/16
 * Time: 13:51
 */

namespace eezeecommerce\RestBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @UniqueEntity(
 *      fields={"code", "supplier"},
 *      errorPath="code",
 *      message="This code already exists"
 *     )
 */
class Trophies
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=300)
     * @Assert\NotBlank
     */
    private $description;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=4, nullable=true)
     */
    private $height;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=4, nullable=true)
     */
    private $length;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=4, nullable=true)
     */
    private $width;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=4, nullable=true)
     */
    private $weight;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=4)
     * @Assert\NotBlank
     */
    private $price;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $has_stock = false;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $stock;

    /**
     * @ORM\Column(type="blob", nullable=true)
     */
    private $image;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $supplier;

    /**
     * @ORM\OneToMany(targetEntity="Trophies", mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="Trophies", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Trophies
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Trophies
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set height
     *
     * @param string $height
     *
     * @return Trophies
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return string
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set length
     *
     * @param string $length
     *
     * @return Trophies
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get length
     *
     * @return string
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set width
     *
     * @param string $width
     *
     * @return Trophies
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return string
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Trophies
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set hasStock
     *
     * @param boolean $hasStock
     *
     * @return Trophies
     */
    public function setHasStock($hasStock)
    {
        $this->has_stock = $hasStock;

        return $this;
    }

    /**
     * Get hasStock
     *
     * @return boolean
     */
    public function getHasStock()
    {
        return $this->has_stock;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     *
     * @return Trophies
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Trophies
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set supplier
     *
     * @param string $supplier
     *
     * @return Trophies
     */
    public function setSupplier($supplier)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return string
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Add child
     *
     * @param \eezeecommerce\RestBundle\Entity\Trophies $child
     *
     * @return Trophies
     */
    public function addChild(\eezeecommerce\RestBundle\Entity\Trophies $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \eezeecommerce\RestBundle\Entity\Trophies $child
     */
    public function removeChild(\eezeecommerce\RestBundle\Entity\Trophies $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \eezeecommerce\RestBundle\Entity\Trophies $parent
     *
     * @return Trophies
     */
    public function setParent(\eezeecommerce\RestBundle\Entity\Trophies $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \eezeecommerce\RestBundle\Entity\Trophies
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set weight
     *
     * @param string $weight
     *
     * @return Trophies
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return string
     */
    public function getWeight()
    {
        return $this->weight;
    }
}
