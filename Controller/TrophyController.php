<?php

namespace eezeecommerce\RestBundle\Controller;

use eezeecommerce\RestBundle\Entity\Trophies;
use eezeecommerce\RestBundle\Form\TrophiesPutType;
use eezeecommerce\RestBundle\Form\TrophiesType;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class Product Api
 * @package eezeecommerce\RestBundle\Controller
 */
class TrophyController extends FOSRestController
{
    /**
     * Show all trophy that are available via the API. If supplier has logged in, it will only
     * show products specific to that company
     *
     * @ApiDoc(
     *     resource=true,
     *     section="trophy",
     *     description="GET all trophy",
     *     statusCodes={
     *         200 = "Returned when supplier has products",
     *         204 = "Returned when supplier has no products"
     *     },
     *     output="eezeecommerce\RestBundle\Form\Example\TrophiesType",
     *     tags={
     *          "stable" = "#5cb85c"
     *     }
     *
     * )
     *
     * @Route("/{_format}/trophy",
     *     defaults={"_format": "json"},
     *     requirements= {
     *          "_format": "json|xml"
     *     }
     * )
     * @Method({"GET"})
     */
    public function showAllAction($_format)
    {
        $products = $this->getDoctrine()->getRepository("eezeecommerceRestBundle:Trophies")->findAll();

        if (count($products) < 1) {
            $view = $this->view($products, 204);

            return $this->handleView($view);
        }

        $view = $this->view($products, 200)->setFormat($_format);

        return $this->handleView($view);
    }

    /**
     * Show all specific trophy by product code if supplier is logged in
     * show product specific to that company to avoid incorrect data to be
     * sent over
     *
     * @ApiDoc(
     *     resource=true,
     *     section="trophy",
     *     description="GET a trophy by code",
     *     statusCodes={
     *         200 = "Returned when supplier has products",
     *         404 = "Returned when product not found"
     *     },
     *     requirements={
     *          {
     *              "name"="code",
     *              "dataType"="string",
     *              "requirements"="string",
     *              "description"="Product Code"
     *          }
     *     },
     *     output="eezeecommerce\RestBundle\Form\Example\TrophiesType",
     *     tags={
     *          "stable" = "#5cb85c"
     *     }
     *
     * )
     * @Route("/{_format}/trophy/{code}", name="show_product",
     *     defaults={"_format": "json"},
     *     requirements= {
     *          "_format": "xml|json"
     *     }
     * )
     * @Method({"GET"})
     */
    public function showAction($_format, $code)
    {
        $product = $this->getDoctrine()->getRepository("eezeecommerceRestBundle:Trophies")
            ->findOneByCode($code);

        if (count($product) < 1) {
            $view = $this->view($product, 404);

            return $this->handleView($view);
        }

        $view = $this->view($product, 200)->setFormat($_format);

        return $this->handleView($view);
    }

    /**
     * Edit a trophy by the product code - Specific to supplier only
     *
     * @ApiDoc(
     *     resource=true,
     *     section="trophy",
     *     description="Edit a trophy",
     *     statusCodes={
     *         200 = "Returned when supplier has products",
     *         400 = "Returned when trophy information sent is invalid",
     *         404 = "Returned when trophy not found"
     *     },
     *     requirements={
     *          {
     *              "name"="code",
     *              "dataType"="string",
     *              "requirements"="string",
     *              "description"="Product Code"
     *          }
     *     },
     *     input="eezeecommerce\RestBundle\Form\Example\TrophiesType",
     *     output="eezeecommerce\RestBundle\Form\Example\TrophiesType",
     *     tags={
     *          "stable" = "#5cb85c"
     *     }
     *
     * )
     * @Route("/{_format}/trophy/{code}",
     *     defaults={"_format": "json"},
     *     requirements= {
     *          "_format": "xml|json"
     *     }
     * )
     * @Method({"PUT"})
     */
    public function editAction($_format, $code, Request $request) {

        $product = $this->getDoctrine()->getRepository("eezeecommerceRestBundle:Trophies")
            ->findOneByCode($code);

        if (count($product) < 1) {
            $view = $this->view($product, 404)->setFormat($_format);

            return $this->handleView($view);
        }

        $form = $this->createForm(new TrophiesPutType($this->getDoctrine()->getManager()), $product);

        $form->handleRequest($request);

        $validator = $this->get("validator");
        $errors = $validator->validate($product);

        if (count($errors) > 0) {
            $view = $this->view($errors, 400)->setFormat($_format);
            return $this->handleView($view);
        }

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            $view = $this->view($product, 200)->setFormat($_format);

            return $this->handleView($view);
        }



        $view = $this->view($form->getData(), 400)->setFormat($_format);

        return $this->handleView($view);
    }

    /**
     * Create a trophy - Specific to supplier only
     *
     * @ApiDoc(
     *     resource=true,
     *     section="trophy",
     *     description="Create a trophy",
     *     statusCodes={
     *         201 = "Returned when trophy has been created",
     *         400 = "Returned when trophy information sent is invalid"
     *     },
     *     input="eezeecommerce\RestBundle\Form\Example\TrophiesType",
     *     tags={
     *          "stable" = "#5cb85c"
     *     }
     *
     * )
     * @Route("/{_format}/trophy",
     *     defaults={"_format": "json"},
     *     requirements= {
     *          "_format": "xml|json"
     *     }
     * )
     * @Method({"POST"})
     */
    public function createAction($_format, Request $request)
    {
        $product = new Trophies();

        $form = $this->createForm(new TrophiesType($this->getDoctrine()->getManager()), $product);

        $form->handleRequest($request);

        $validator = $this->get("validator");
        $errors = $validator->validate($product);

        if (count($errors) > 0) {
            $view = $this->view($errors, 400)->setFormat($_format);
            return $this->handleView($view);
        }

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            $response = new Response();
            $response->setStatusCode(201);

            $response->headers->set("Location",
                $this->generateUrl(
                    "show_product", array("code" => $product->getCode()),
                    true
                )
            );

            return $response;
        }


        $view = $this->view($form->getData(), 400)->setFormat($_format);

        return $this->handleView($view);
    }

    /**
     * Delete a trophy by the product code - Specific to supplier only
     *
     * @ApiDoc(
     *     resource=true,
     *     section="trophy",
     *     description="Delete a given trophy",
     *     statusCodes={
     *         200 = "Returned when supplier has trophy",
     *         409 = "Returned when trophy deleted has children. The child must be deleted first",
     *         404 = "Returned when trophy not found"
     *     },
     *     requirements={
     *          {
     *              "name"="code",
     *              "dataType"="string",
     *              "requirements"="string",
     *              "description"="Product Code"
     *          }
     *     },
     *     tags={
     *          "stable" = "#5cb85c"
     *     }
     *
     * )
     * @Route("/{_format}/trophy/{code}",
     *     defaults={"_format": "json"},
     *     requirements= {
     *          "_format": "xml|json"
     *     }
     * )
     * @Method({"DELETE"})
     */
    public function deleteAction($_format, $code)
    {
        $product = $this->getDoctrine()->getRepository("eezeecommerceRestBundle:Trophies")
            ->findOneByCode($code);

        if (count($product) < 1) {
            $view = $this->view($product, 404)->setFormat($_format);
            return $this->handleView($view);
        }

        if (count($product->getChildren()) > 0) {
            $view = $this->view($product, 409)->setFormat($_format);
            return $this->handleView($view);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($product);
        $em->flush();


        $view = $this->view($product, 204)->setFormat($_format);

        return $this->handleView($view);
    }

}
